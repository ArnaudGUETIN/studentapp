package com.app.MV.entities;

import com.app.entities.Matiere;
import com.app.entities.Note;
import com.app.entities.Student;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;

public class Moyenne {
    private Matiere matiere;
    private Student student;
    private BigDecimal average;
    private BigDecimal note1;
    private BigDecimal note2;
    private BigDecimal note3;

    public Moyenne(Matiere matiere, Student student) {
        this.matiere = new Matiere();
        this.matiere.setCoeficient(matiere.getCoeficient());
        this.matiere.setLibelle(matiere.getLibelle());
        this.matiere.setMatiereId(matiere.getMatiereId());
        this.matiere.setProfessorName(matiere.getProfessorName());
        this.student = new Student();
        this.student.setStudentId(student.getStudentId());
        this.student.setName(student.getName());
        this.student.setSurname(student.getSurname());
        this.student.setNotes(student.getNotes());
    }

    public Matiere getMatiere() {
        return matiere;
    }

    public void setMatiere(Matiere matiere) {
        this.matiere = matiere;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public BigDecimal getAverage() {
        average=ZERO;
        BigDecimal quotient=ZERO;
        BigDecimal notes= ZERO;
        if(matiere!=null && student!=null && !CollectionUtils.isEmpty(student.getNotes())){
            int compteur=0;
            for(Note note:student.getNotes()){
                if(note.getDevoir().getMatiere().getMatiereId().equals(matiere.getMatiereId())){
                    BigDecimal value = valueOf(note.getValue());
                    BigDecimal coeficient = valueOf(note.getDevoir().getMatiere().getCoeficient());
                    notes = notes.add(value.multiply(coeficient));
                    quotient = quotient.add(coeficient);
                     compteur++;
                     if(compteur==1){
                         this.note1=value;
                     }else if(compteur==2){
                         this.note2=value;
                     }else if(compteur==3){
                         this.note3=value;
                     }

                }
            }
        }
        average =quotient!=ZERO && notes!=ZERO?notes.divide(quotient, 2, RoundingMode.HALF_UP):ZERO;
        return average;
    }

    public void setAverage(BigDecimal average) {
        this.average = average;
    }

    public BigDecimal getNote1() {
        return note1;
    }

    public void setNote1(BigDecimal note1) {
        this.note1 = note1;
    }

    public BigDecimal getNote2() {
        return note2;
    }

    public void setNote2(BigDecimal note2) {
        this.note2 = note2;
    }

    public BigDecimal getNote3() {
        return note3;
    }

    public void setNote3(BigDecimal note3) {
        this.note3 = note3;
    }
}
