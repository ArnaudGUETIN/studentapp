package com.app;

import com.app.dao.AnneeScolaireRepository;
import com.app.dao.ClasseStudentRepository;
import com.app.entities.AnneeScolaire;
import com.app.security.AppRole;
import com.app.security.AppUser;
import com.app.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Date;

@SpringBootApplication
public class StudentAppApplication implements CommandLineRunner {

	@Autowired
	AccountService accountService;
	@Autowired
	AnneeScolaireRepository anneeScolaireRepository;
	@Autowired
	ClasseStudentRepository classeStudentRepository;
	@Bean
	public BCryptPasswordEncoder getBCPE() {

		return new BCryptPasswordEncoder();
	}

	@Override
	public void run(String... args) {
		AppUser appUser = new AppUser();
		appUser.setUsername("guetin.arnaud@gmail.com");
		appUser.setPassword("1234");
		appUser.setDateCreation(new Date());
		AppRole appRole = new AppRole();

		appRole.setRoleName("ADMIN");

		AnneeScolaire anneeScolaire =  new AnneeScolaire();
		anneeScolaire.setAnneeScolaire("2018/2019");

		//anneeScolaireRepository.save(anneeScolaire);



//		appUser=accountService.CreateUser(appUser);
//		appRole=accountService.CreateRole(appRole);
//		accountService.addRoleToUser(appUser.getUsername(),appRole.getRoleName());
	}

	public static void main(String[] args) {
		SpringApplication.run(StudentAppApplication.class, args);
	}

}

