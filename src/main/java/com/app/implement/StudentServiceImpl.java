package com.app.implement;

import com.app.MV.entities.Moyenne;
import com.app.dao.*;
import com.app.entities.*;
import com.app.services.CalculMoyenneService;
import com.app.services.MatiereService;
import com.app.services.NoteService;
import com.app.services.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

@Service
@Transactional
public class StudentServiceImpl implements StudentService{

	
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private ClasseRepository classeRepository;
	@Autowired
	private EcoleRepository ecoleRepository;
	@Autowired
	private MatiereRepository matiereRepository;
	@Autowired
	private NoteRepository noteRepository;
	@Autowired
	private NoteService noteService;
	@Autowired
	private CalculMoyenneService calculMoyenneService;
	@Autowired
	private MatiereService matiereService;
	@Autowired
	AnneeScolaireRepository anneeScolaireRepository;
	@Autowired
	ClasseStudentRepository classeStudentRepository;

	private AnneeScolaire anneeScolaire;

	private static final Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);
	
	@Override
	public Student getEntity(Long entityId) {
		// TODO Auto-generated method stub
		Student student = studentRepository.getOne(entityId);
		updateMoyennes(student);
		return student;
	}

	private void updateMoyennes(Student student) {
		Map<String, BigDecimal> moyenneMap=new HashMap<>();
		Classe classe= new Classe();
		for(ClasseStudent classeStudent:student.getClasseStudents()){
			if(classeStudent.getAnneeScolaire().equals(anneeScolaire)){
				classe = classeStudent.getClasse();
			}
		}
		if (!CollectionUtils.isEmpty(classe.getMatieres())){
			for(Matiere matiere:classe.getMatieres()){
				BigDecimal moyenne=calculMoyenneService.calculateStudentMoyenneForMatiere(student.getStudentId(),matiere.getMatiereId());
				if(moyenneMap.get(matiere.getLibelle())!=null){
					moyenne = moyenne.add(moyenneMap.get(matiere.getLibelle())).divide(BigDecimal.valueOf(2));
				}
				moyenneMap.put(matiere.getLibelle(),moyenne);
			}
		}

		student.setMoyennesMapLibelle(moyenneMap);
	}

	@Override
	public Student addEntity(Student entity) {
		// TODO Auto-generated method stub
		return studentRepository.save(entity);
	}

	@Override
	public Student updateEntity(Student entity, Long entityId) {
		// TODO Auto-generated method stub
		Student student = studentRepository.findOne(entityId);
		student.setEmail(entity.getEmail());
		student.setMatricule(entity.getMatricule());
		student.setName(entity.getName());
		student.setSurname(entity.getSurname());
		student.setBirthday(entity.getBirthday());
		student.setJoinday(entity.getJoinday());
		student.setNationnalite(entity.getNationnalite());
		student.setAdresse(entity.getAdresse());
		student.setPlaceOfBirth(entity.getPlaceOfBirth());
		student.setAdresse(entity.getAdresse());
		return studentRepository.save(student);
	}

	@Override
	public boolean removeEntity(Long entityId) {
		// TODO Auto-generated method stub
		try {
			studentRepository.delete(entityId);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		return false;
	}

	@Override
	public List<Student> getAllEntities() {
		// TODO Auto-generated method stub
		return studentRepository.findAll();
	}

	@Override
	public List<Student> getAllStudentsOfClasse(Long classeId) {
		// TODO Auto-generated method stub
		Classe classe = classeRepository.getOne(classeId);
		List<Student> students = new ArrayList<>();
		for(ClasseStudent classeStudent:classe.getClasseStudents()){
			if(classeStudent.getStudent()!=null){
				students.add(classeStudent.getStudent());
			}

		}
		return students;
	}

	@Override
	public List<Student> getAllStudentsOfSchool(Long ecoleId) {
		// TODO Auto-generated method stub
		Ecole ecole = ecoleRepository.getOne(ecoleId);
		List<Student> students = new ArrayList<>();
		ecole.getClasses().forEach(classe->{
			students.addAll(getAllStudentsOfClasse(classe.getClasseId()));
		});
		return students;
	}

	@Override
	public void addStudentToClasse(Long studentId, Long classeId) {
		// TODO Auto-generated method stub
		Classe classe = classeRepository.getOne(classeId);
		Student student = studentRepository.getOne(studentId);
		AnneeScolaire anneeScolaire = anneeScolaireRepository.findOne(1L);
		ClasseStudent classeStudent = new ClasseStudent();



		for(ClasseStudent classeStudentItem:student.getClasseStudents()){
			if(classeStudentItem.getAnneeScolaire().equals(anneeScolaire)){
				classeStudentItem.setClasse(classe);
			}
		}

		classe.getMatieres().forEach(matiere -> {
			matiere.getDevoirs().forEach(devoir -> {
				Note note = noteService.addEntity(new Note());
				noteService.addNoteToDevoir(devoir.getDevoirId(),note.getNoteId());
				noteService.addNoteToStudent(studentId,note.getNoteId());
			});

		});
		try {
			classeStudent.setClasse(classe);
			classeStudent.setAnneeScolaire(anneeScolaire);
			classeStudent.setDateInscription(new Date());
			classeStudent.setStudent(studentRepository.save(student));
			classeStudentRepository.save(classeStudent);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

	@Override
	public Double getMatiereAverage(Long matiereId,Long studentId) {
		// TODO Auto-generated method stub
		Matiere matiere = matiereRepository.getOne(matiereId);
		Double toReturn = new Double(0);
		int i=0;
		List<Devoir> devoirs = new ArrayList<>(matiere.getDevoirs());
		for(Devoir devoir:devoirs){
			toReturn = noteRepository
					.findByDevoirDevoirIdAndStudentStudentId(devoir.getDevoirId(), studentId).getValue();
		    i++;
		}
		
		
		return toReturn/i;
	}

	@Override
	public List<Moyenne> getMoyennesList(Long studentId) {
		Student student = studentRepository.getOne(studentId);
		List<Moyenne> moyenneList = new ArrayList<>();
		for(Matiere matiere:matiereService.getMatieres(student)){
           Moyenne moyenne = new Moyenne(matiere,student);
           moyenneList.add(moyenne);
		}
		return moyenneList;
	}

	public AnneeScolaire getAnneeScolaire() {
		return anneeScolaire;
	}

	public void setAnneeScolaire(AnneeScolaire anneeScolaire) {
		this.anneeScolaire = anneeScolaire;
	}
}
