package com.app.implement;

import com.app.dao.AnneeScolaireRepository;
import com.app.entities.AnneeScolaire;
import com.app.services.AnneeScolaireService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class AnneeScolaireImpl implements AnneeScolaireService {
    @Autowired
    AnneeScolaireRepository anneeScolaireRepository;
    private static final Logger logger = LoggerFactory.getLogger(AnneeScolaireImpl.class);

    @Override
    public AnneeScolaire getEntity(Long entityId) {
        return anneeScolaireRepository.findOne(entityId);
    }

    @Override
    public AnneeScolaire addEntity(AnneeScolaire entity) {
        return anneeScolaireRepository.save(entity);
    }

    @Override
    public AnneeScolaire updateEntity(AnneeScolaire entity, Long entityId) {
        AnneeScolaire anneeScolaire = anneeScolaireRepository.findOne(entityId);
        anneeScolaire.setAnneeScolaire(entity.getAnneeScolaire());
        anneeScolaire.setCurrentAnneeScolaire(entity.isCurrentAnneeScolaire());


        return anneeScolaireRepository.save(anneeScolaire);
    }

    @Override
    public boolean removeEntity(Long entityId) {
        try {
            anneeScolaireRepository.delete(entityId);
            return true;
        } catch (Exception e) {
            // TODO: handle exception
            logger.error(e.getStackTrace().toString() );
        }
        return false;
    }

    @Override
    public List<AnneeScolaire> getAllEntities() {
        return anneeScolaireRepository.findAll();
    }

    @Override
    public AnneeScolaire updateCurrentAnneeScolaire(Long id) {
        AnneeScolaire anneeScolaireModified = anneeScolaireRepository.findOne(id);
        for(AnneeScolaire anneeScolaire:getAllEntities()){
            if(anneeScolaire.getAnneeScolaireId().equals(id)){
                anneeScolaire.setCurrentAnneeScolaire(true);
                anneeScolaireModified=anneeScolaire;
            }else {
                anneeScolaire.setCurrentAnneeScolaire(false);
            }

        }
        return anneeScolaireRepository.save(anneeScolaireModified);
    }
}
