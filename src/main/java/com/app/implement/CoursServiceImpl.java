package com.app.implement;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ClasseRepository;
import com.app.dao.CoursRepository;
import com.app.dao.MatiereRepository;
import com.app.entities.Classe;
import com.app.entities.Cours;
import com.app.entities.Matiere;
import com.app.services.CoursService;

@Service
@Transactional
public class CoursServiceImpl implements CoursService{

	@Autowired
	private CoursRepository coursRepository;
	@Autowired
	private MatiereRepository matiereRepository;
	@Autowired
	private ClasseRepository classeRepository;
	private static final Logger logger = LoggerFactory.getLogger(CoursServiceImpl.class);
	
	@Override
	public Cours getEntity(Long entityId) {
		// TODO Auto-generated method stub
		logger.info("**** getting cours entity****");
		return coursRepository.getOne(entityId);
	}

	@Override
	public Cours addEntity(Cours entity) {
		// TODO Auto-generated method stub
		logger.info("**** adding cours entity****");
		return coursRepository.save(entity);
	}

	@Override
	public Cours updateEntity(Cours entity, Long entityId) {
		// TODO Auto-generated method stub
		logger.info("**** updating cours entity****");
		Cours cours = coursRepository.findOne(entityId);
		cours.setHeureDebut(entity.getHeureDebut());
		cours.setHeureFin(entity.getHeureFin());
		return coursRepository.save(cours);
	}

	@Override
	public boolean removeEntity(Long entityId) {
		// TODO Auto-generated method stub
		try {
			coursRepository.delete(entityId);
			logger.info("**** removing cours entity****");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		return false;
	}

	@Override
	public List<Cours> getAllEntities() {
		// TODO Auto-generated method stub
		logger.info("**** getting all cours****");
		return coursRepository.findAll();
	}

	@Override
	public List<Cours> getAllCoursOfClasse(Long classeId) {
		// TODO Auto-generated method stub
		logger.info("**** getting all cours of classe****");
		Classe classe = classeRepository.getOne(classeId);
		return new ArrayList<>(classe.getCours());
	}

	@Override
	public List<Cours> getAllCoursOfMatiere(Long matiereId) {
		// TODO Auto-generated method stub
		logger.info("**** getting all cours of matiere****");
		Matiere matiere = matiereRepository.getOne(matiereId);
		return new ArrayList<>(matiere.getCours());
	}

	@Override
	public void addCoursToClasse(Long coursId, Long classeId) {
		// TODO Auto-generated method stub
		Classe classe = classeRepository.findOne(classeId);
		Cours cours = coursRepository.findOne(coursId);
		
		
		try {
			cours.setClasse(classe);
			cours=coursRepository.save(cours);
			logger.info("**** adding cours to classe****");
			System.out.println(cours.getClasse());
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

	@Override
	public void addCoursToMatiere(Long coursId, Long matiereId) {
		// TODO Auto-generated method stub
		Matiere matiere = matiereRepository.findOne(matiereId);
		Cours cours = coursRepository.findOne(coursId);
		matiere.setCouleur(cours.getCouleur());
		
		
		try {
			cours.setMatiere(matiere);
			coursRepository.save(cours);
			matiereRepository.save(matiere);
			logger.info("**** adding cours to matiere****");
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

}
