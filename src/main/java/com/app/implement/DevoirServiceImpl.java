package com.app.implement;

import com.app.dao.ClasseRepository;
import com.app.dao.DevoirRepository;
import com.app.dao.MatiereRepository;
import com.app.entities.*;
import com.app.services.DevoirService;
import com.app.services.NoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DevoirServiceImpl implements DevoirService{

	@Autowired
	private DevoirRepository devoirRepository;
	@Autowired
	private MatiereRepository matiereRepository;
	@Autowired
	private ClasseRepository classeRepository;
	@Autowired
	private NoteService noteService;
	private static final Logger logger = LoggerFactory.getLogger(DevoirServiceImpl.class);
	
	
	@Override
	public Devoir getEntity(Long entityId) {
		// TODO Auto-generated method stub
		logger.info("**** get devoir entity****");
		return devoirRepository.getOne(entityId);
	}

	@Override
	public Devoir addEntity(Devoir entity) {
		// TODO Auto-generated method stub
		logger.info("**** adding devoir entity****");
		return devoirRepository.save(entity);
	}

	@Override
	public Devoir updateEntity(Devoir entity, Long entityId) {
		// TODO Auto-generated method stub
		entity.setDevoirId(entityId);
		logger.info("**** updating devoir entity****");
		return devoirRepository.save(entity);
	}

	@Override
	public boolean removeEntity(Long entityId) {
		// TODO Auto-generated method stub
		try {
			devoirRepository.delete(entityId);
			logger.info("**** deleting devoir entity****");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		return false;
	}

	@Override
	public List<Devoir> getAllEntities() {
		// TODO Auto-generated method stub
		logger.info("**** getting all devoirs****");
		return devoirRepository.findAll();
	}

	@Override
	public List<Devoir> getAllDevoirsOfMatiere(Long matiereId) {
		// TODO Auto-generated method stub
		Matiere matiere = matiereRepository.getOne(matiereId);
		logger.info("**** getting all devoirs  of matiere****");
		return new ArrayList<>(matiere.getDevoirs());
	}

	@Override
	public void addDevoirToMatiere(Long devoirId, Long matiereId,Long classeId) {
		// TODO Auto-generated method stub
		Matiere matiere = matiereRepository.getOne(matiereId);
		Devoir devoir = devoirRepository.getOne(devoirId);
		Classe classe = classeRepository.findOne(classeId);
		List<Student> students = new ArrayList<>();
		for(ClasseStudent classeStudent:classe.getClasseStudents()){
			students.add(classeStudent.getStudent());
		}
		students.forEach(student -> {
			Note note=noteService.addEntity(new Note());
			noteService.addNoteToDevoir(devoirId,note.getNoteId());
			noteService.addNoteToStudent(student.getStudentId(),note.getNoteId());
		});
	    devoir.setMatiere(matiere);
	    try {
			devoirRepository.saveAndFlush(devoir);
			logger.info("**** adding devoir to matiere****");
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

}
