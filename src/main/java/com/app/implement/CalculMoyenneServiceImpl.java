package com.app.implement;

import com.app.dao.MatiereRepository;
import com.app.dao.StudentRepository;
import com.app.entities.Matiere;
import com.app.entities.Note;
import com.app.entities.Student;
import com.app.services.CalculMoyenneService;
import com.app.services.MatiereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.BigDecimal.*;
@Service
@Transactional
public class CalculMoyenneServiceImpl implements CalculMoyenneService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private MatiereRepository matiereRepository;
    @Autowired
    private MatiereService matiereService;

    @Override
    public BigDecimal calculateStudentMoyenne(Long studentId) {
        return null;
    }

    @Override
    public BigDecimal calculateStudentMoyenneForMatiere(Long studentId, Long matiereId) {
        Student student=studentRepository.findOne(studentId);
        Matiere matiere=matiereRepository.findOne(matiereId);
        Double quotient=0D;
        Double notes= 0D;
        for(Note note:student.getNotes()){
            if(note.getDevoir().getMatiere().getMatiereId().equals(matiereId)){
                Double value = note.getValue();
                Double coeficient = note.getDevoir().getMatiere().getCoeficient();
                notes += notes*coeficient;
                quotient += coeficient;
            }
        }

        return quotient!=0 ?valueOf(notes/quotient):ZERO;
    }
}
