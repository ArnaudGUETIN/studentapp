package com.app.implement;

import com.app.dao.RoleRepository;
import com.app.dao.UserRepository;
import com.app.security.AppRole;
import com.app.security.AppUser;
import com.app.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MailConstructor mailConstructor;

//    @Autowired
//    private JavaMailSender mailSender;

    @Override
    public AppUser CreateUser(AppUser user) {
        // TODO Auto-generated method stub
        String password = user.getPassword();
        String HPW = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(HPW);
        user.setDateCreation(new Date());
        user = userRepository.save(user);
        SimpleMailMessage email = mailConstructor.constructNewUserEmail(user, password);
        //mailSender.send(email);
        return user;
    }

    @Override
    public AppRole CreateRole(AppRole role) {
        // TODO Auto-generated method stub
        return roleRepository.save(role);
    }

    @Override
    public void addRoleToUser(String username, String roleName) {
        // TODO Auto-generated method stub
        AppRole role = roleRepository.findByRoleName(roleName);
        AppUser user = userRepository.findByUsername(username);
        user.getRoles().add(role);
    }

    @Override
    public AppUser findUserByUsername(String username) {
        // TODO Auto-generated method stub
        return userRepository.findByUsername(username);
    }

    @Override
    public AppUser updateUser(String username) {
        AppUser appUser = findUserByUsername(username);
        appUser.setDateDerniereConnexion(new Date());
        appUser.setId(appUser.getId());
        return userRepository.save(appUser);
    }
}
