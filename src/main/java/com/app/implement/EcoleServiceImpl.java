package com.app.implement;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.EcoleRepository;
import com.app.entities.Ecole;
import com.app.services.EcoleService;

@Service
@Transactional
public class EcoleServiceImpl implements EcoleService{

	@Autowired
	private EcoleRepository ecoleRepository;
	private static final Logger logger = LoggerFactory.getLogger(EcoleServiceImpl.class);
	
	
	@Override
	public Ecole getEntity(Long entityId) {
		// TODO Auto-generated method stub
		logger.info("***** getting ecole entity****");
		return ecoleRepository.getOne(entityId);
	}

	@Override
	public Ecole addEntity(Ecole entity) {
		// TODO Auto-generated method stub
		logger.info("****** adding ecole entity*****");
		return ecoleRepository.save(entity);
	}

	@Override
	public Ecole updateEntity(Ecole entity, Long entityId) {
		// TODO Auto-generated method stub
		logger.info("*****updating ecole entity *****");
		entity.setEcoleId(entityId);
		return ecoleRepository.save(entity);
	}

	@Override
	public boolean removeEntity(Long entityId) {
		// TODO Auto-generated method stub
		try {
			ecoleRepository.delete(entityId);
			logger.info("*****removing ecole entity *****");
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		return false;
	}

	@Override
	public List<Ecole> getAllEntities() {
		// TODO Auto-generated method stub
		logger.info("*****getting all ecole entitties *****");
		return ecoleRepository.findAll();
	}

}
