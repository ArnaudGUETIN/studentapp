package com.app.implement;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.EcoleRepository;
import com.app.dao.ProfessorRepository;
import com.app.entities.Ecole;
import com.app.entities.Professor;
import com.app.services.ProfessorService;


@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService{

	@Autowired
	private ProfessorRepository professorRepository;
	@Autowired
	private EcoleRepository ecoleRepository;
	private static final Logger logger = LoggerFactory.getLogger(ProfessorServiceImpl.class);
	
	@Override
	public Professor getEntity(Long entityId) {
		// TODO Auto-generated method stub
		return professorRepository.getOne(entityId);
	}

	@Override
	public Professor addEntity(Professor entity) {
		// TODO Auto-generated method stub
		return professorRepository.save(entity);
	}

	@Override
	public Professor updateEntity(Professor entity, Long entityId) {
		// TODO Auto-generated method stub
		entity.setProfessorId(entityId);
		Professor professor = professorRepository.getOne(entityId);
		professor.setName(entity.getName());
		professor.setSurname(entity.getSurname());
		professor.setEmail(entity.getEmail());
		return professorRepository.save(professor);
	}

	@Override
	public boolean removeEntity(Long entityId) {
		// TODO Auto-generated method stub
		try {
			professorRepository.delete(entityId);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		return false;
	}

	@Override
	public List<Professor> getAllEntities() {
		// TODO Auto-generated method stub
		return professorRepository.findAll();
	}

	@Override
	public List<Professor> getAllProfessorsOfSchool(Long ecoleId) {
		// TODO Auto-generated method stub
		Ecole ecole=ecoleRepository.getOne(ecoleId);
		return new ArrayList<>(ecole.getProfessors());
	}

	@Override
	public Professor addProfessorToSchool(Long ecoleId, Long professorId) {
		// TODO Auto-generated method stub
		Ecole ecole=ecoleRepository.getOne(ecoleId);
		Professor professor =professorRepository.getOne(professorId);
		
		professor.setEcole(ecole);
		try {
			professorRepository.saveAndFlush(professor);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		return null;
	}

}
