package com.app.implement;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.DevoirRepository;
import com.app.dao.MatiereRepository;
import com.app.dao.NoteRepository;
import com.app.dao.StudentRepository;
import com.app.entities.Devoir;
import com.app.entities.Matiere;
import com.app.entities.Note;
import com.app.entities.Student;
import com.app.services.NoteService;

@Service
@Transactional
public class NoteServiceImpl implements NoteService{

	@Autowired
	private NoteRepository noteRepository;
	@Autowired
	private MatiereRepository matiereRepository;
	@Autowired
	private DevoirRepository devoirRepository;
	@Autowired
	private StudentRepository studentRepository;
	private static final Logger logger = LoggerFactory.getLogger(NoteServiceImpl.class);
	
	@Override
	public Note getEntity(Long entityId) {
		// TODO Auto-generated method stub
		return noteRepository.getOne(entityId);
	}

	@Override
	public Note addEntity(Note entity) {
		// TODO Auto-generated method stub
		return noteRepository.save(entity);
	}

	@Override
	public Note updateEntity(Note entity, Long entityId) {
		// TODO Auto-generated method stub
		entity.setNoteId(entityId);
		return noteRepository.save(entity);
	}

	@Override
	public boolean removeEntity(Long entityId) {
		// TODO Auto-generated method stub
		try {
			noteRepository.delete(entityId);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		return false;
	}

	@Override
	public List<Note> getAllEntities() {
		// TODO Auto-generated method stub
		return noteRepository.findAll();
	}

	@Override
	public List<Note> getAllNotesOfStudent(Long matiereId, Long studentId) {
		// TODO Auto-generated method stub
		List<Note> notes = new ArrayList<>();
		Matiere matiere = matiereRepository.getOne(matiereId);
		matiere.getDevoirs().forEach(devoir->{
			notes.add(noteRepository.findByDevoirDevoirIdAndStudentStudentId(devoir.getDevoirId(), studentId));
		});
		return notes;
	}

	@Override
	public List<Note> getAllNotesOfDevoir(Long devoirId) {
		// TODO Auto-generated method stub
		Devoir devoir = devoirRepository.getOne(devoirId);
		return new ArrayList<>(devoir.getNotes());
	}

	@Override
	public void addNoteToDevoir(Long devoirId, Long noteId) {
		// TODO Auto-generated method stub
		Note note = noteRepository.getOne(noteId);
		Devoir devoir = devoirRepository.getOne(devoirId);
		note.setDevoir(devoir);
		try {
			noteRepository.save(note);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

	@Override
	public void addNoteToStudent(Long studentId, Long noteId) {
		// TODO Auto-generated method stub
		Note note = noteRepository.getOne(noteId);
		Student student = studentRepository.getOne(studentId);
		note.setStudent(student);
		try {
			noteRepository.save(note);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

	@Override
	public List<Note> updateNotes(List<Note> notes) {
		List<Note> toReturn = new ArrayList<>();
		for(Note note:notes){
			Note noteItem = noteRepository.findOne(note.getNoteId());
			noteItem.setValue(note.getValue());
			toReturn.add(noteRepository.save(noteItem));
		}
		return toReturn;
	}

}
