package com.app.implement;

import java.util.*;

import com.app.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ClasseRepository;
import com.app.dao.MatiereRepository;
import com.app.dao.ProfessorRepository;
import com.app.services.MatiereService;

@Service
@Transactional
public class MatiereServiceImpl implements MatiereService{

	@Autowired
	private MatiereRepository matiereRepository;
	@Autowired
	private ClasseRepository classeRepository;
	@Autowired
	private ProfessorRepository professorRepository;
	private static final Logger logger = LoggerFactory.getLogger(MatiereServiceImpl.class);
	
	
	@Override
	public Matiere getEntity(Long entityId) {
		// TODO Auto-generated method stub
		return matiereRepository.getOne(entityId);
	}

	@Override
	public Matiere addEntity(Matiere entity) {
		// TODO Auto-generated method stub
		return matiereRepository.save(entity);
	}

	@Override
	public Matiere updateEntity(Matiere entity, Long entityId) {
		// TODO Auto-generated method stub
		Matiere matiere = matiereRepository.getOne(entityId);
		matiere.setCoeficient(entity.getCoeficient());
		matiere.setLibelle(entity.getLibelle());
		return matiereRepository.save(matiere);
	}

	@Override
	public boolean removeEntity(Long entityId) {
		// TODO Auto-generated method stub
		try {
			matiereRepository.delete(entityId);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		return false;
	}

	@Override
	public List<Matiere> getAllEntities() {
		// TODO Auto-generated method stub
		Map<String,Matiere> matieresMap=new HashMap<>();
		List<Matiere> matieres = matiereRepository.findAll();
		matieres.forEach(mat->{
			if(matieresMap.get(mat.getLibelle())==null) {
				matieresMap.put(mat.getLibelle(), mat);
			}
		});
		return new ArrayList<Matiere>(matieresMap.values());
	}

	@Override
	public List<Matiere> getAllMatieresOfClasse(Long classeId) {
		// TODO Auto-generated method stub
		Classe classe = classeRepository.getOne(classeId);
		return new ArrayList<>(classe.getMatieres());
	}

	@Override
	public List<Matiere> getAllMatieresOfProfessor(Long professorId) {
		// TODO Auto-generated method stub
		Professor professor = professorRepository.getOne(professorId);
		return new ArrayList<>(professor.getMatieres());
	}

	@Override
	public void addMatiereToProfessor(Long professorId, Long matiereId) {
		// TODO Auto-generated method stub
		Matiere matiere = matiereRepository.getOne(matiereId);
		Professor professor = professorRepository.getOne(professorId);
		matiere.setProfessor(professor);
		try {
			matiereRepository.save(matiere);
		} catch (Exception e) {
			// TODO: handle exception
			logger.info(e.getStackTrace().toString());
		}
		
	}

	@Override
	public void addMatiereToClasse(Long classeId, Long matiereId) {
		// TODO Auto-generated method stub
		Matiere matiere = matiereRepository.getOne(matiereId);
		Classe classe = classeRepository.getOne(classeId);
		
		matiere.getClasses().add(classe);
		try {
			matiereRepository.save(matiere);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

	@Override
	public List<Matiere> getMatieres(Student student) {
		Set<Matiere> matieres=new HashSet<>();

		for(Note note:student.getNotes()){
			matieres.add(note.getDevoir().getMatiere());
		}
		return new ArrayList<>(matieres);
	}

	@Override
	public List<Matiere> filterMatieresById(Student student, Long matiereId) {
		Set<Matiere> matieres=new HashSet<>();
		for(Note note:student.getNotes()){
			if(note.getDevoir().getMatiere().getMatiereId().equals(matiereId)){
				matieres.add(note.getDevoir().getMatiere());
			}
		}
		return new ArrayList<>(matieres);
	}

}
