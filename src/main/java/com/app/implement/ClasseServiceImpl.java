package com.app.implement;

import com.app.dao.*;
import com.app.entities.Classe;
import com.app.entities.Ecole;
import com.app.entities.Professor;
import com.app.services.ClasseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class ClasseServiceImpl implements ClasseService{

	@Autowired
	private ClasseRepository  classeRepository;
	@Autowired
	private EcoleRepository  ecoleRepository;
	@Autowired
	AnneeScolaireRepository anneeScolaireRepository;
	@Autowired
	ClasseStudentRepository classeStudentRepository;
	@Autowired
	private ProfessorRepository  professorRepository;
	private static final Logger logger = LoggerFactory.getLogger(ClasseServiceImpl.class);
	
	@Override
	public Classe getEntity(Long entityId) {
		// TODO Auto-generated method stub
		return classeRepository.getOne(entityId);
	}

	@Override
	public Classe addEntity(Classe entity) {
		// TODO Auto-generated method stub
		return classeRepository.save(entity);
	}

	@Override
	public Classe updateEntity(Classe entity, Long entityId) {
		// TODO Auto-generated method stub
		Classe classe = classeRepository.findOne(entityId);
		classe.setLibelle(entity.getLibelle());
		classe.setCommentaire(entity.getCommentaire());
		classe.setDescription(entity.getDescription());

		
		return classeRepository.save(classe);
	}

	@Override
	public boolean removeEntity(Long entityId) {
		// TODO Auto-generated method stub
		try {
			classeRepository.delete(entityId);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString() );
		}
		return false;
	}

	@Override
	public List<Classe> getAllEntities() {
		// TODO Auto-generated method stub
        logger.info("*********** getting all classes entitties********");		
		return classeRepository.findAll();
	}

	@Override
	public List<Classe> getAllClassesOfSchool(Long ecoleId) {
		// TODO Auto-generated method stub
		Ecole ecole=ecoleRepository.getOne(ecoleId);
		return new ArrayList<>(ecole.getClasses());
	}

	@Override
	public List<Classe> getAllClassesOfProfessor(Long professorId) {
		// TODO Auto-generated method stub
		Professor professor = professorRepository.getOne(professorId);
		return new ArrayList<>(professor.getClasses());
	}

	@Override
	public void addClasseToSchool(Long ecoleId, Long classeId) {
		// TODO Auto-generated method stub
		Ecole ecole = ecoleRepository.getOne(ecoleId);
		Classe classe = classeRepository.getOne(classeId);
		
		classe.setEcole(ecole);
		try {
			classeRepository.save(classe);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

	@Override
	public void addClasseToProfessor(Long professorId, Long classeId) {
		// TODO Auto-generated method stub
		Professor professor = professorRepository.getOne(professorId);
		Classe classe = classeRepository.getOne(classeId);
		classe.getProfessors().add(professor);
	
		try {
			classeRepository.save(classe);
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getStackTrace().toString());
		}
		
	}

}
