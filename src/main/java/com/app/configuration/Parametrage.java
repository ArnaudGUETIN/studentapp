package com.app.configuration;

import com.app.entities.Ecole;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "t_parametrage")
public class Parametrage implements Serializable {

    private static int FIRST_DEVOIR=1;
    private static int SECOND_DEVOIR=2;
    private static int THIRD_DEVOIR=3;

    @Id
    private Long parametrageId;
    private String label;
    private boolean valeur;
    private int type;
    private Ecole ecole;

    public Long getParametrageId() {
        return parametrageId;
    }

    public void setParametrageId(Long parametrageId) {
        this.parametrageId = parametrageId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isValeur() {
        return valeur;
    }

    public void setValeur(boolean valeur) {
        this.valeur = valeur;
    }
}
