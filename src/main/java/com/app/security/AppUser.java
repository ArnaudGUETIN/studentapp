package com.app.security;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class AppUser implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private String username;
    private String password;
    private Date dateCreation;
    private Date dateDerniereConnexion;
    @Transient
    private String currentRole;
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    private List<AppRole> roles;


    public AppUser() {
        super();
    }


    public AppUser(String username, String password, Date dateCreation, List<AppRole> roles) {
        super();
        this.username = username;
        this.password = password;
        this.dateCreation = dateCreation;
        this.roles = roles;
    }


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public Date getDateCreation() {
        return dateCreation;
    }


    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }


    public Date getDateDerniereConnexion() {
        return dateDerniereConnexion;
    }


    public void setDateDerniereConnexion(Date dateDerniereConnexion) {
        this.dateDerniereConnexion = dateDerniereConnexion;
    }


    public List<AppRole> getRoles() {
        return roles;
    }


    public void setRoles(List<AppRole> roles) {
        this.roles = roles;
    }


    public String getCurrentRole() {
        return currentRole;
    }


    public void setCurrentRole(String currentRole) {
        this.currentRole = currentRole;
    }


}
