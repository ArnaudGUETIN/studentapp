package com.app.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Table(name="t_note")
@Entity
public class Note implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long noteId;
	private Double value=0D;
	private Double quotient=1D;
	@JsonIgnoreProperties(value = "notes", allowSetters=true)
    @JoinColumn(name = "devoirId", referencedColumnName = "devoirId", insertable = true, updatable = true)
    @ManyToOne
	private Devoir devoir;
	@JsonIgnoreProperties(value = "notes", allowSetters=true)
    @JoinColumn(name = "studentId", referencedColumnName = "studentId", insertable = true, updatable = true)
    @ManyToOne
	private Student student;
	public Long getNoteId() {
		return noteId;
	}
	public void setNoteId(Long noteId) {
		this.noteId = noteId;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getQuotient() {
		return quotient;
	}
	public void setQuotient(Double quotient) {
		this.quotient = quotient;
	}
	public Devoir getDevoir() {
		return devoir;
	}
	public void setDevoir(Devoir devoir) {
		this.devoir = devoir;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}


	

}
