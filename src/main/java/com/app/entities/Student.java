package com.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name="t_student")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Student implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long studentId;
	@Column(length=256,nullable=false)
	private String name;
	@Column(length=256,nullable=false)
	private String surname;
	@Column(length=256,nullable=false)
	private String email;
	@Temporal(TemporalType.TIMESTAMP)
	private Date birthday;
	@Column(length=128)
	private String placeOfBirth;
	@Temporal(TemporalType.TIMESTAMP)
	private Date joinday;
	@Column(length=256)
	private String matricule;
	@Column(length=128)
	private String nationnalite;
	@JsonIgnore
	@OneToMany(mappedBy = "student")
	private Set<ClasseStudent> classeStudents;
	@Embedded
	private Adresse adresse;
	@Transient
	private Map<Matiere, BigDecimal> moyennesMap;
	@Transient
	private Map<String, BigDecimal> moyennesMapLibelle;
	@JsonIgnoreProperties(value = "student", allowSetters=true)
	@OneToMany(mappedBy="student",fetch=FetchType.LAZY)
	private Set<Note> notes;
	public Long getStudentId() {
		return studentId;
	}
	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Set<ClasseStudent> getClasseStudents() {
		return classeStudents;
	}

	public void setClasseStudents(Set<ClasseStudent> classeStudents) {
		this.classeStudents = classeStudents;
	}

	public Adresse getAdresse() {
		return adresse;
	}
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	public Set<Note> getNotes() {
		return notes;
	}
	public void setNotes(Set<Note> notes) {
		this.notes = notes;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public Date getJoinday() {
		return joinday;
	}

	public void setJoinday(Date joinday) {
		this.joinday = joinday;
	}

	public String getNationnalite() {
		return nationnalite;
	}

	public void setNationnalite(String nationnalite) {
		this.nationnalite = nationnalite;
	}

	public Map<Matiere, BigDecimal> getMoyennesMap() {
		return moyennesMap;
	}
	@JsonIgnore
	public void setMoyennesMap(Map<Matiere, BigDecimal> moyennesMap) {
		this.moyennesMap = moyennesMap;
	}

	public Map<String, BigDecimal> getMoyennesMapLibelle() {
		return moyennesMapLibelle;
	}
	@JsonIgnore
	public void setMoyennesMapLibelle(Map<String, BigDecimal> moyennesMapLibelle) {
		this.moyennesMapLibelle = moyennesMapLibelle;
	}
}
