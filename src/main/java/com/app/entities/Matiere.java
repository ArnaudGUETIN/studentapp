package com.app.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.app.services.AppUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Table(name="t_matiere")
@Entity
public class Matiere implements AppUtils,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long matiereId;
	@Column(length=256,nullable=false)
	private String libelle;
	@Column(length=30)
	private String couleur;
	@OneToMany(mappedBy="matiere",fetch=FetchType.LAZY,cascade = CascadeType.REMOVE)
	private Set<Cours> cours;
	@JsonIgnoreProperties(value = "matiere", allowSetters=true)
	@OneToMany(mappedBy="matiere",fetch=FetchType.LAZY,cascade = CascadeType.REMOVE)
	private Set<Devoir> devoirs;
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.PERSIST)
	private Set<Classe> classes;
	@OrderBy("name")
	@JsonIgnore
	@ManyToOne
	private Professor professor;
	private double coeficient;
	@Transient
	private String professorName;
	@Transient
	private Long professorId;
	
	public Long getMatiereId() {
		return matiereId;
	}
	public void setMatiereId(Long matiereId) {
		this.matiereId = matiereId;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Set<Devoir> getDevoirs() {
		return devoirs;
	}
	public void setDevoirs(Set<Devoir> devoirs) {
		this.devoirs = devoirs;
	}
	public Set<Classe> getClasses() {
		return classes;
	}
	public void setClasses(Set<Classe> classes) {
		this.classes = classes;
	}
	public Set<Cours> getCours() {
		return cours;
	}
	public void setCours(Set<Cours> cours) {
		this.cours = cours;
	}
	public Professor getProfessor() {
		return professor;
	}
	public void setProfessor(Professor professor) {
		this.professor = professor;
	}
	public String getCouleur() {
		return couleur;
	}
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	public String getProfessorName() {
		if(this.professor!=null) {
			professorName = this.professor.getName()+" "+this.professor.getSurname();
		}
		return professorName;
	}
	public void setProfessorName(String professorName) {
		this.professorName = professorName;
	}
	public double getCoeficient() {
		return coeficient;
	}
	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}
	public Long getProfessorId() {
		if(this.professor!=null) {
			professorId = this.professor.getProfessorId();
		}
		return professorId;
	}
	public void setProfessorId(Long professorId) {
		this.professorId = professorId;
	}
	

}
