package com.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
@Table(name = "t_classe_student")
@Entity
public class ClasseStudent implements Serializable {

    @Id
    @GeneratedValue
    private Long classeStudentId;
    @JsonIgnore
    @JoinColumn(name = "student_id")
    @ManyToOne
    private Student student;
    @JsonIgnore
    @JoinColumn(name = "classe_id")
    @ManyToOne
    private Classe classe;
    @JsonIgnore
    @JoinColumn(name = "annee_scolaire_id")
    @ManyToOne
    private AnneeScolaire anneeScolaire;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInscription;

    public Long getClasseStudentId() {
        return classeStudentId;
    }

    public void setClasseStudentId(Long classeStudentId) {
        this.classeStudentId = classeStudentId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Classe getClasse() {
        return classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public Date getDateInscription() {
        return dateInscription;
    }

    public void setDateInscription(Date dateInscription) {
        this.dateInscription = dateInscription;
    }

    public AnneeScolaire getAnneeScolaire() {
        return anneeScolaire;
    }

    public void setAnneeScolaire(AnneeScolaire anneeScolaire) {
        this.anneeScolaire = anneeScolaire;
    }
}
