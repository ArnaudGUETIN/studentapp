package com.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Table(name = "t_annee_scolaire")
@Entity
public class AnneeScolaire implements Serializable {

    @Id
    @GeneratedValue
    private Long anneeScolaireId;
    private String anneeScolaire;
    private boolean currentAnneeScolaire;
    @OneToMany(mappedBy = "anneeScolaire")
    private Set<ClasseStudent> classeStudents;

    public Long getAnneeScolaireId() {
        return anneeScolaireId;
    }

    public void setAnneeScolaireId(Long anneeScolaireId) {
        this.anneeScolaireId = anneeScolaireId;
    }

    public String getAnneeScolaire() {
        return anneeScolaire;
    }

    public void setAnneeScolaire(String anneeScolaire) {
        this.anneeScolaire = anneeScolaire;
    }

    public Set<ClasseStudent> getClasseStudents() {
        return classeStudents;
    }

    public void setClasseStudents(Set<ClasseStudent> classeStudents) {
        this.classeStudents = classeStudents;
    }

    public boolean isCurrentAnneeScolaire() {
        return currentAnneeScolaire;
    }

    public void setCurrentAnneeScolaire(boolean currentAnneeScolaire) {
        this.currentAnneeScolaire = currentAnneeScolaire;
    }
}
