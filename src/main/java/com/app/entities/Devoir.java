package com.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Table(name="t_devoir")
@Entity
public class Devoir implements Serializable{

	private static int FIRST_DEVOIR=1;
	private static int SECOND_DEVOIR=2;
	private static int THIRD_DEVOIR=3;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long devoirId;
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	private String libelle;
	@Transient
	private String matiereName;
	@Transient
	private String classeName;
	@JsonIgnoreProperties(value = "devoir", allowSetters=true)
	@OneToMany(mappedBy="devoir",fetch=FetchType.LAZY)
	private Set<Note> notes;
	@JsonIgnoreProperties(value = "devoirs", allowSetters=true)
	@ManyToOne
	private Matiere matiere;
	private int devoirOrder;
	public Long getDevoirId() {
		return devoirId;
	}
	public void setDevoirId(Long devoirId) {
		this.devoirId = devoirId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Set<Note> getNotes() {
		return notes;
	}
	public void setNotes(Set<Note> notes) {
		this.notes = notes;
	}
	public Matiere getMatiere() {
		return matiere;
	}
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public int getDevoirOrder() {
		return devoirOrder;
	}

	public void setDevoirOrder(int devoirOrder) {
		this.devoirOrder = devoirOrder;
	}

	public String getLibelle() {

		if(getDevoirOrder()==FIRST_DEVOIR){
			libelle = "1er Devoir";
		}else if (getDevoirOrder()==SECOND_DEVOIR){
			libelle = "2ème Devoir";
		}else if (getDevoirOrder()==THIRD_DEVOIR){
			libelle = "3ème Devoir";
		}
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getMatiereName() {
		if(matiere!=null){
			matiereName=matiere.getLibelle();
		}
		return matiereName;
	}

	public void setMatiereName(String matiereName) {
		this.matiereName = matiereName;
	}

	public String getClasseName() {
		List<Note> noteList = new ArrayList<>(notes);
		if(!CollectionUtils.isEmpty(noteList)){
			List<ClasseStudent> classeStudents = new ArrayList<>(noteList.get(0).getStudent().getClasseStudents());
			classeName= classeStudents.get(0).getClasse().getLibelle();
		}
		return classeName;
	}

	public void setClasseName(String classeName) {
		this.classeName = classeName;
	}
}
