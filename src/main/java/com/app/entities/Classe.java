package com.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Table(name="t_classe")
@Entity
public class Classe implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long classeId;
	@Column(length=24,nullable=false)
	private String libelle;
    @Column(length=256)
	private String commentaire;
    @Column(length=256)
	private String description;
	@JsonIgnore
	@ManyToOne
	private Ecole ecole;
	@JsonIgnore
	@ManyToMany
	private Set<Professor> professors;
	@OneToMany(mappedBy = "classe")
	private Set<ClasseStudent> classeStudents;
	@OrderBy("libelle")
	@ManyToMany(mappedBy="classes",fetch=FetchType.LAZY,cascade = CascadeType.PERSIST)
	private Set<Matiere> matieres;
	@OneToMany(mappedBy="classe",fetch=FetchType.LAZY)
	private Set<Cours> cours;
	@Transient
	private int effectif;
	@Transient
	private String schoolName;
	
	public Long getClasseId() {
		return classeId;
	}
	public void setClasseId(Long classeId) {
		this.classeId = classeId;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Ecole getEcole() {
		return ecole;
	}
	public void setEcole(Ecole ecole) {
		this.ecole = ecole;
	}

	public Set<ClasseStudent> getClasseStudents() {
		return classeStudents;
	}

	public void setClasseStudents(Set<ClasseStudent> classeStudents) {
		this.classeStudents = classeStudents;
	}

	public Set<Matiere> getMatieres() {
		return matieres;
	}
	public void setMatieres(Set<Matiere> matieres) {
		this.matieres = matieres;
	}
	public Set<Cours> getCours() {
		return cours;
	}
	public void setCours(Set<Cours> cours) {
		this.cours = cours;
	}
	public Set<Professor> getProfessors() {
		return professors;
	}
	public void setProfessors(Set<Professor> professors) {
		this.professors = professors;
	}
	public int getEffectif() {
		if(getClasseStudents()!=null){
			effectif = getClasseStudents().size();
		}
		return effectif;
	}

	public void setEffectif(int effectif) {
		this.effectif = effectif;
	}

    public String getSchoolName() {
	    if(getEcole()!=null){
	        schoolName=getEcole().getNom();
        }
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
