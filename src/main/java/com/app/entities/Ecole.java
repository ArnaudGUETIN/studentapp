package com.app.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;


@Table(name="t_ecole")
@Entity
public class Ecole implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long ecoleId;
	private String nom;
	@Embedded
	private Adresse adresse;
	@OrderBy("name")
	@OneToMany(mappedBy="ecole",fetch=FetchType.LAZY)
	private Set<Professor> professors;
	@OrderBy("libelle")
	@OneToMany(mappedBy="ecole",fetch=FetchType.LAZY)
	private Set<Classe> classes;
	public Long getEcoleId() {
		return ecoleId;
	}
	public void setEcoleId(Long ecoleId) {
		this.ecoleId = ecoleId;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Adresse getAdresse() {
		return adresse;
	}
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	public Set<Professor> getProfessors() {
		return professors;
	}
	public void setProfessors(Set<Professor> professors) {
		this.professors = professors;
	}
	public Set<Classe> getClasses() {
		return classes;
	}
	public void setClasses(Set<Classe> classes) {
		this.classes = classes;
	}
	

}
