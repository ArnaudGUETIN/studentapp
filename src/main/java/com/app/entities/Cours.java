package com.app.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.app.services.AppUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Table(name="t_cours")
@Entity
public class Cours implements AppUtils,  Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long coursId;
	private String libelle;
	@Temporal(TemporalType.TIMESTAMP)
	private Date heureDebut;
	@Temporal(TemporalType.TIMESTAMP)
	private Date heureFin;
	
	@JsonIgnore
    @JoinColumn(name = "classeId", referencedColumnName = "classeId")
    @ManyToOne
	private Classe classe;
	@JsonIgnore
    @JoinColumn(name = "matiereId", referencedColumnName = "matiereId")
    @ManyToOne
	private Matiere matiere;
	private String couleur;
	public Long getCoursId() {
		return coursId;
	}
	public void setCoursId(Long coursId) {
		this.coursId = coursId;
	}
	public Date getHeureDebut() {
		return heureDebut;
	}
	public void setHeureDebut(Date heureDebut) {
		this.heureDebut = heureDebut;
	}
	public Date getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(Date heureFin) {
		this.heureFin = heureFin;
	}
	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	public Matiere getMatiere() {
		return matiere;
	}
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	public String getLibelle() {
		if(this.matiere!=null) {
			libelle=getMatiere().getLibelle();
		}
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	@Override
	public String getCouleur() {
		// TODO Auto-generated method stub
		if(this.matiere!=null && couleur==null) {
			couleur=this.matiere.getCouleur();
		}
		return couleur;
	}
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	
	

}
