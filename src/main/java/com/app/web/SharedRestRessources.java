package com.app.web;

import com.app.entities.Ecole;
import com.app.services.EcoleService;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/shared")
@CrossOrigin("*")
public class SharedRestRessources {

    @Autowired
    private EcoleService ecoleService;

    @RequestMapping(value ="ecole/{id}")
    public Object getAllMetrics(@PathVariable("id") Long ecoleId){
        Ecole ecole = ecoleService.getEntity(ecoleId);
        JsonObject jsonObject =new JsonObject();

        jsonObject.addProperty("classeEffectif", ecole.getClasses().size());
        jsonObject.addProperty("professorEffectif", ecole.getProfessors().size());

        return jsonObject.toString();
    }

}
