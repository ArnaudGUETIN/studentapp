package com.app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Professor;
import com.app.services.ProfessorService;

@RestController
@RequestMapping(value="/professors")
@CrossOrigin("*")
public class ProfessorRestRessources {

	@Autowired
	private ProfessorService professorService;
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<Professor> getAllProfessors() {
		System.out.println("***************ici*********");
		return professorService.getAllEntities();
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public Professor getProfessor(@PathVariable("id")Long professorId) {
		System.out.println("***************ici*********");
		return professorService.getEntity(professorId);
	}
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Professor addProfessor(@RequestBody Professor professor) {
		return professorService.addEntity(professor);
	}
	
	@RequestMapping(value="/update",method=RequestMethod.PUT)
	public Professor updateClasse(@RequestBody Professor professor) {
		return professorService.updateEntity(professor, professor.getProfessorId());
	}
	
	@RequestMapping(value="/remove/{id}",method=RequestMethod.DELETE)
	public boolean removeProfessor(@PathVariable("id") Long professorId) {
		return professorService.removeEntity(professorId);
	} 
	
	@RequestMapping(value="/school/{ecoleId}/professors/all",method=RequestMethod.GET)
	public List<Professor> getAllSchoolProfessors(@PathVariable("ecoleId") Long ecoleId){
		return professorService.getAllProfessorsOfSchool(ecoleId);
	}
	
	@RequestMapping(value="/{professorId}/addTo/school/{ecoleId}",method=RequestMethod.GET)
	public void addProfessorToSchool(@PathVariable("ecoleId") Long ecoleId,@PathVariable("professorId") Long professorId) {
		 professorService.addProfessorToSchool(ecoleId, professorId);
	}
}
