package com.app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Note;
import com.app.services.NoteService;



@RestController
@RequestMapping(name="")
public class NoteRestRessources {

	@Autowired
	private NoteService noteService;
	
	
	@RequestMapping(value="/notes/all",method=RequestMethod.GET)
	public List<Note> getAllNotes() {
		System.out.println("***************ici*********");
		return noteService.getAllEntities();
	}
	
	@RequestMapping(value="/notes/{id}",method=RequestMethod.GET)
	public Note getNote(@PathVariable("id")Long noteId) {
		System.out.println("***************ici*********");
		return noteService.getEntity(noteId);
	}
	
	@RequestMapping(value="/notes/add/devoir/{devoirId}/student/{studentId}",method=RequestMethod.POST)
	public Note addNote(@RequestBody Note note,
			@PathVariable("devoirId") Long devoirId,
			@PathVariable("studentId") Long studentId ) {
		Note added = noteService.addEntity(note);
		noteService.addNoteToDevoir(devoirId, added.getNoteId());
		noteService.addNoteToStudent(studentId, added.getNoteId());
		return added;
	}
	
	@RequestMapping(value="/notes/update",method=RequestMethod.PUT)
	public Note updateNote(@RequestBody Note note) {
		return noteService.updateEntity(note, note.getNoteId());
	}

    @RequestMapping(value="/notes/update/all",method=RequestMethod.PUT)
    public List<Note> updateNote(@RequestBody List<Note> notes) {
        return noteService.updateNotes(notes);
    }

	@RequestMapping(value="/notes/remove/{id}",method=RequestMethod.DELETE)
	public boolean removeNote(@PathVariable("id") Long noteId) {
		return noteService.removeEntity(noteId);
	}
	
	@RequestMapping(value="/notes/student/{studentId}/matiere/{matiereId}/notes/all",method=RequestMethod.GET)
	public List<Note> getAllStudentNotes(@PathVariable("matiereId") Long matiereId,@PathVariable("studentId") Long studentId){
		return noteService.getAllNotesOfStudent(matiereId, studentId);
	}
	@RequestMapping(value="/notes/devoir/{devoirId}/notes/all",method=RequestMethod.GET)
	public List<Note> getAllDevoirNotes(@PathVariable("devoirId") Long devoirId){
		return noteService.getAllNotesOfDevoir(devoirId);
	}
	
}
