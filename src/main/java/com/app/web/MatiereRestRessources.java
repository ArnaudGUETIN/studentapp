package com.app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Matiere;
import com.app.services.MatiereService;



@RestController
@RequestMapping(name="")
public class MatiereRestRessources {

	@Autowired
	private MatiereService matiereService;
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/all",method=RequestMethod.GET)
	public List<Matiere> getAllMatieres() {
		System.out.println("***************ici*********");
		return matiereService.getAllEntities();
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/{id}",method=RequestMethod.GET)
	public Matiere getMatiere(@PathVariable("id")Long matiereId) {
		System.out.println("***************ici*********");
		return matiereService.getEntity(matiereId);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/add",method=RequestMethod.POST)
	public Matiere addMatiere(@RequestBody Matiere matiere) {
		return matiereService.addEntity(matiere);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/update",method=RequestMethod.PUT)
	public Matiere updateMatiere(@RequestBody Matiere matiere) {
		return matiereService.updateEntity(matiere, matiere.getMatiereId());
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/remove/{id}",method=RequestMethod.DELETE)
	public boolean removeMatiere(@PathVariable("id") Long matiereId) {
		return matiereService.removeEntity(matiereId);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/classe/{classeId}/matieres/all",method=RequestMethod.GET)
	public List<Matiere> getAllClasseMatieres(@PathVariable("classeId") Long classeId){
		return matiereService.getAllMatieresOfClasse(classeId);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/professor/{professorId}/classes/all",method=RequestMethod.GET)
	public List<Matiere> getAllProfessorMatieres(@PathVariable("professorId") Long professorId){
		return matiereService.getAllMatieresOfProfessor(professorId);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/classe/{classeId}/addTo/matiere/{matiereId}",method=RequestMethod.GET)
	public void addMatiereToClasse(@PathVariable("matiereId") Long matiereId,@PathVariable("classeId") Long classeId) {
		 matiereService.addMatiereToClasse(classeId, matiereId);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matieres/professor/{professorId}/addTo/matiere/{matiereId}",method=RequestMethod.GET)
	public void addMatiereToProfessor(@PathVariable("matiereId") Long matiereId,@PathVariable("professorId") Long professorId) {
		 matiereService.addMatiereToProfessor(professorId, matiereId);
	}
}
