package com.app.web;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import com.app.MV.entities.Moyenne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.app.entities.Student;
import com.app.services.StudentService;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping(value="/students")
@CrossOrigin("*")
public class StudentRestRessources {

	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<Student> getAllStudents() {
		return studentService.getAllEntities();
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public Student getStudent(@PathVariable("id")Long studentId) {
		return studentService.getEntity(studentId);
	}
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Student addStudent(@RequestBody Student student) {
		return studentService.addEntity(student);
	}
	
	@RequestMapping(value="/update",method=RequestMethod.PUT)
	public Student updateStudent(@RequestBody Student student) {
		return studentService.updateEntity(student, student.getStudentId());
	}
	
	@RequestMapping(value="/remove/{id}",method=RequestMethod.DELETE)
	public boolean removeStudent(@PathVariable("id") Long studentId) {
		return studentService.removeEntity(studentId);
	}
	
	@RequestMapping(value="/classe/{classeId}/students/all",method=RequestMethod.GET)
	public List<Student> getAllClasseStudents(@PathVariable("classeId") Long classeId){
		return studentService.getAllStudentsOfClasse(classeId);
	}
	@RequestMapping(value="/school/{schoolId}/students/all",method=RequestMethod.GET)
	public List<Student> getAllSchoolStudent(@PathVariable("schoolId") Long schoolId){
		return studentService.getAllStudentsOfSchool(schoolId);
	}
	
	@RequestMapping(value="/{studentId}/addTo/classe/{classeId}",method=RequestMethod.GET)
	public void addStudentToClasse(@PathVariable("studentId") Long studentId,@PathVariable("classeId") Long classeId) {
		 studentService.addStudentToClasse(studentId, classeId);
	}
	
	@RequestMapping(value="/{studentId}/average/matiere/{matiereId}",method=RequestMethod.GET)
	public void getMatiereAverage(@PathVariable("matiereId") Long matiereId,@PathVariable("studentId") Long studentId) {
		 studentService.getMatiereAverage(matiereId, studentId);
	}

	@RequestMapping(value="/moyennes/{id}",method=RequestMethod.GET)
	public List<Moyenne> getMoyennesOfStudent(@PathVariable("id")Long studentId) {
		return studentService.getMoyennesList(studentId);
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/add/image", method = RequestMethod.POST)
	public ResponseEntity upload(
			@RequestParam("id") Long id,
			HttpServletResponse response, HttpServletRequest request
	) {
		try {
			@SuppressWarnings("unused")
			Student student = studentService.getEntity(id);
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Iterator<String> it = multipartRequest.getFileNames();
			MultipartFile multipartFile = multipartRequest.getFile(it.next());
			String fileName = id + ".png";


			byte[] bytes = multipartFile.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("src/main/resources/static/image/student/" + fileName)));
			stream.write(bytes);
			stream.close();

			return new ResponseEntity("Upload Success!", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity("Upload failed!", HttpStatus.BAD_REQUEST);
		}
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@RequestMapping(value = "/update/image", method = RequestMethod.POST)
	public ResponseEntity updateImagePost(
			@RequestParam("id") Long id,
			HttpServletResponse response, HttpServletRequest request
	) {
		try {
			Student student = studentService.getEntity(id);
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			Iterator<String> it = multipartRequest.getFileNames();
			MultipartFile multipartFile = multipartRequest.getFile(it.next());
			String fileName = id + ".png";

			Files.delete(Paths.get("src/main/resources/static/image/student/" + fileName));

			byte[] bytes = multipartFile.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File("src/main/resources/static/image/student/" + fileName)));
			stream.write(bytes);
			stream.close();

			return new ResponseEntity("Upload Success!", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity("Upload failed!", HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/image/{id}", method = RequestMethod.GET)
	public @ResponseBody
	byte[] getImage(@PathVariable("id") String id) throws IOException {
		try {
			Path path = Paths.get("src/main/resources/static/image/student/" + id + ".png");
			byte[] data = Files.readAllBytes(path);

			return data;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//System.out.println("image not exist");
			return null;
		}
	}
}
