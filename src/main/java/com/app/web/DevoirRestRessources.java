package com.app.web;

import java.util.List;

import com.app.entities.Matiere;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.app.entities.Devoir;
import com.app.services.DevoirService;


@RestController
@RequestMapping(value="/devoirs")
public class DevoirRestRessources {

	@Autowired
	private DevoirService devoirService;

	@CrossOrigin("*")
	@RequestMapping(value="/{id}",method= RequestMethod.GET)
	public Devoir getDevoir(@PathVariable("id")Long devoirId) {
		return devoirService.getEntity(devoirId);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/all",method= RequestMethod.GET)
	public List<Devoir> getAllDevoirs(){
		return devoirService.getAllEntities();
	}
	@CrossOrigin("*")
	@RequestMapping(value="/add",method= RequestMethod.POST)
	public Devoir addDevoir(@RequestBody Devoir devoir) {
		return devoirService.addEntity(devoir);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/update",method= RequestMethod.PUT)
	public Devoir updateDevoir(@RequestBody Devoir devoir){
		return devoirService.updateEntity(devoir,devoir.getDevoirId());
	}
	@CrossOrigin("*")
	@RequestMapping(value="/matiere/{matiereId}/devoirs/all",method= RequestMethod.GET)
	public List<Devoir> getAllDevoirsOfMatiere(@PathVariable("matiereId") Long matiereId){
		return devoirService.getAllDevoirsOfMatiere(matiereId);
	}

	@CrossOrigin("*")
	@RequestMapping(value="/devoir/{devoirId}/addTo/matiere/{matiereId}/classe/{classeId}",method=RequestMethod.GET)
	public void addMatiereToClasse(@PathVariable("matiereId") Long matiereId,@PathVariable("devoirId") Long devoirId,@PathVariable("classeId") Long classeId) {
		devoirService.addDevoirToMatiere(devoirId, matiereId,classeId);
	}
}
