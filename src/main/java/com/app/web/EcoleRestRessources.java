package com.app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Ecole;
import com.app.services.EcoleService;

@RestController
@RequestMapping(value="/ecoles")
@CrossOrigin("*")
public class EcoleRestRessources {

	@Autowired
	private EcoleService ecoleService;
	
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<Ecole> getAllSchool() {
		return ecoleService.getAllEntities();
	}
	
	@RequestMapping(value="/{ecoleId}",method=RequestMethod.GET)
	public Ecole getEcole(@PathVariable("ecoleId")Long ecoleId) {
		return ecoleService.getEntity(ecoleId);
	}
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Ecole addSchool(@RequestBody Ecole ecole) {
		return ecoleService.addEntity(ecole);
	}
	
	@RequestMapping(value="/update",method=RequestMethod.PUT)
	public Ecole updateEcole(@RequestBody Ecole ecole) {
		return ecoleService.updateEntity(ecole, ecole.getEcoleId());
	}
	
	@RequestMapping(value="/remove/{ecoleId}",method=RequestMethod.DELETE)
	public boolean removeEcole(@PathVariable("ecoleId") Long ecoleId) {
		return ecoleService.removeEntity(ecoleId);
	}
}
