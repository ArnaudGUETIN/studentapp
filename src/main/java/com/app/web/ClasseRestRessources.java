package com.app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Classe;
import com.app.services.ClasseService;

@RestController
@RequestMapping(value="/classes")
@CrossOrigin("*")
public class ClasseRestRessources {
	
	@Autowired
	private ClasseService classeService;
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/all",method=RequestMethod.GET)
	public List<Classe> getAllClasses() {
		System.out.println("***************ici*********");
		return classeService.getAllEntities();
	}
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/{id}",method=RequestMethod.GET)
	public Classe getClasse(@PathVariable("id")Long classeId) {
		System.out.println("***************ici*********");
		return classeService.getEntity(classeId);
	}
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public Classe addClasse(@RequestBody Classe classe) {
		return classeService.addEntity(classe);
	}
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/update",method=RequestMethod.PUT)
	public Classe updateClasse(@RequestBody Classe classe) {
		return classeService.updateEntity(classe, classe.getClasseId());
	}
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/remove/{id}",method=RequestMethod.DELETE)
	public boolean removeClasse(@PathVariable("id") Long classeId) {
		return classeService.removeEntity(classeId);
	}
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/school/{ecoleId}/classes/all",method=RequestMethod.GET)
	public List<Classe> getAllSchoolClasses(@PathVariable("ecoleId") Long ecoleId){
		return classeService.getAllClassesOfSchool(ecoleId);
	}
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/professor/{professorId}/classes/all",method=RequestMethod.GET)
	public List<Classe> getAllProfessorClasses(@PathVariable("professorId") Long professorId){
		return classeService.getAllClassesOfProfessor(professorId);
	}
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/school/{ecoleId}/addTo/classe/{classeId}",method=RequestMethod.GET)
	public void addClasseToSchool(@PathVariable("ecoleId") Long ecoleId,@PathVariable("classeId") Long classeId) {
		 classeService.addClasseToSchool(ecoleId, classeId);
	}
	@CrossOrigin(allowedHeaders = "*")
	@RequestMapping(value="/professor/{professorId}/addTo/classe/{classeId}",method=RequestMethod.GET)
	public void addClasseToProfessor(@PathVariable("professorId") Long professorId,@PathVariable("classeId") Long classeId) {
		 classeService.addClasseToProfessor(professorId, classeId);
	}

}
