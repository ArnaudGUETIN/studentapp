package com.app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Cours;
import com.app.services.CoursService;

@RestController
@CrossOrigin("*")
public class CoursRestRessources {

	@Autowired
	private CoursService coursService;
	
	@RequestMapping(value="/cours/all",method=RequestMethod.GET)
	public List<Cours> getAllCours() {
		return coursService.getAllEntities();
	}
	@RequestMapping(value="/cours/{id}",method=RequestMethod.GET)
	public Cours getCours(@PathVariable("id")Long coursId) {
		return coursService.getEntity(coursId);
	}
	@RequestMapping(value="/cours/add",method=RequestMethod.POST)
	public Cours addCours(@RequestBody Cours cours) {
		System.out.println(cours);
		return coursService.addEntity(cours);
	}
	@RequestMapping(value="/cours/update",method=RequestMethod.PUT)
	public Cours updateCours(@RequestBody Cours cours) {
		return coursService.updateEntity(cours, cours.getCoursId());
	}
	@RequestMapping(value="/cours/remove/{id}",method=RequestMethod.DELETE)
	public boolean removeCours(@PathVariable("id")Long coursId) {
		return coursService.removeEntity(coursId);
	}
	@CrossOrigin("*")
	@RequestMapping(value="/cours/classe/{id}/cours/all",method=RequestMethod.GET)
	public List<Cours> getAllClasseCours(@PathVariable("id")Long classeId){
		return coursService.getAllCoursOfClasse(classeId);
	}
	@RequestMapping(value="/cours/matiere/{id}/cours/all",method=RequestMethod.GET)
	public List<Cours> getAllMatiereCours(@PathVariable("id")Long matiereId){
		return coursService.getAllCoursOfMatiere(matiereId);
	}
	@RequestMapping(value="/cours/classe/{classeId}/addTo/cours/{coursId}",method=RequestMethod.GET)
	public void addCoursToClasse(@PathVariable("classeId")Long classeId,@PathVariable("coursId")Long coursId) {
		coursService.addCoursToClasse(coursId, classeId);
	}
	@RequestMapping(value="/cours/matiere/{matiereId}/addTo/cours/{coursId}",method=RequestMethod.GET)
	public void addCoursToMatiere(@PathVariable("matiereId")Long matiereId,@PathVariable("coursId")Long coursId) {
		coursService.addCoursToMatiere(coursId, matiereId);
	}
}
