package com.app.web;

import com.app.entities.AnneeScolaire;
import com.app.services.AnneeScolaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/anneeScolaires")
@CrossOrigin("*")
public class AnneeScolaireRestRessources {
    @Autowired
    private AnneeScolaireService anneeScolaireService;
    @CrossOrigin(allowedHeaders = "*")
    @RequestMapping(value="/all",method= RequestMethod.GET)
    public List<AnneeScolaire> getAllAnneeScolaire() {
        System.out.println("***************ici*********");
        return anneeScolaireService.getAllEntities();
    }
    @CrossOrigin(allowedHeaders = "*")
    @RequestMapping(value="/{id}",method=RequestMethod.GET)
    public AnneeScolaire getAnneeScolaire(@PathVariable("id")Long anneeScolaireId) {
        System.out.println("***************ici*********");
        return anneeScolaireService.getEntity(anneeScolaireId);
    }
    @CrossOrigin(allowedHeaders = "*")
    @RequestMapping(value="/add",method=RequestMethod.POST)
    public AnneeScolaire addAnneeScolaire(@RequestBody AnneeScolaire anneeScolaire) {
        return anneeScolaireService.addEntity(anneeScolaire);
    }
    @CrossOrigin(allowedHeaders = "*")
    @RequestMapping(value="/update",method=RequestMethod.PUT)
    public AnneeScolaire updateAnneeScolaire(@RequestBody AnneeScolaire anneeScolaire) {
        return anneeScolaireService.updateEntity(anneeScolaire, anneeScolaire.getAnneeScolaireId());
    }
    @CrossOrigin(allowedHeaders = "*")
    @RequestMapping(value="/updateCurrentAnneeScolaire/{anneeScolaireId}",method=RequestMethod.GET)
    public AnneeScolaire updateCurrentAnneeScolaire(@PathVariable("anneeScolaireId") Long anneeScolaireId) {
        return anneeScolaireService.updateCurrentAnneeScolaire(anneeScolaireId);
    }
    @CrossOrigin(allowedHeaders = "*")
    @RequestMapping(value="/remove/{id}",method=RequestMethod.DELETE)
    public boolean removeAnneeScolaire(@PathVariable("id") Long anneeScolaireId) {
        return anneeScolaireService.removeEntity(anneeScolaireId);
    }
}
