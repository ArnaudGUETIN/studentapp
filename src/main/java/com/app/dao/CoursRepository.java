package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.app.entities.Cours;
@RestResource(exported = false)
public interface CoursRepository extends JpaRepository<Cours, Long>{

}
