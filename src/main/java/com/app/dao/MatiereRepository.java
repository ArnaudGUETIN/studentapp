package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.app.entities.Matiere;
@RestResource(exported = false)
public interface MatiereRepository extends JpaRepository<Matiere, Long>{

}
