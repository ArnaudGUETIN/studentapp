package com.app.dao;

import com.app.entities.AnneeScolaire;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnneeScolaireRepository extends JpaRepository<AnneeScolaire,Long> {
}
