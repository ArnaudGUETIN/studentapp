package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.app.entities.Ecole;
@RestResource(exported = false)
public interface EcoleRepository extends JpaRepository<Ecole, Long>{

}
