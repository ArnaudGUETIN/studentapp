package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.app.entities.Student;
@RestResource(exported = false)
public interface StudentRepository extends JpaRepository<Student, Long>{

	
}
