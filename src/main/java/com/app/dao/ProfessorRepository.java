package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.app.entities.Professor;
@RestResource(exported = false)
public interface ProfessorRepository extends JpaRepository<Professor, Long> {

}
