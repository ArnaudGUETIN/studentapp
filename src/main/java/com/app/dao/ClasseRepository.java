package com.app.dao;

import com.app.entities.Classe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
@RestResource(exported = false)
public interface ClasseRepository extends JpaRepository<Classe, Long>{

//	public Classe findByStudentsStudentId(Long studentId);
//	public Classe findByCoursCoursId(Long coursId);
}
