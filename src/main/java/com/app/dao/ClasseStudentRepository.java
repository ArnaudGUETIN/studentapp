package com.app.dao;

import com.app.entities.ClasseStudent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClasseStudentRepository extends JpaRepository<ClasseStudent,Long> {
    public ClasseStudent findByClasseClasseId(Long classeId);
}
