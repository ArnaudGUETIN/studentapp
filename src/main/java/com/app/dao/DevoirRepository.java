package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.app.entities.Devoir;
@RestResource(exported = false)
public interface DevoirRepository extends JpaRepository<Devoir, Long>{

}
