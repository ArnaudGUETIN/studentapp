package com.app.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.app.entities.Note;
@RestResource(exported = false)
public interface NoteRepository extends JpaRepository<Note, Long>{
	
	Note findByDevoirDevoirIdAndStudentStudentId(Long devoirId,Long studentId);
}
