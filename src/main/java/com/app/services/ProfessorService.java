package com.app.services;

import java.util.List;

import com.app.entities.Professor;

public interface ProfessorService extends CommonService<Professor>{

	List<Professor> getAllProfessorsOfSchool(Long ecoleId);
	Professor addProfessorToSchool(Long ecoleId,Long professorId);
}
