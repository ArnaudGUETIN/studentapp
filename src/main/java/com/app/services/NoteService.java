package com.app.services;

import java.util.List;

import com.app.entities.Note;

public interface NoteService extends CommonService<Note>{

	List<Note> getAllNotesOfStudent(Long matiereId,Long studentId);
	List<Note> getAllNotesOfDevoir(Long devoirId);
	public void addNoteToDevoir(Long devoirId,Long noteId);
	public void addNoteToStudent(Long studentId,Long noteId);
	public List<Note> updateNotes(List<Note> notes);
}
