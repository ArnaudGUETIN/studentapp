package com.app.services;

import java.util.List;

import com.app.MV.entities.Moyenne;
import com.app.entities.Student;

public interface StudentService extends CommonService<Student>{

	
	public List<Student> getAllStudentsOfClasse(Long classeId);
	public List<Student> getAllStudentsOfSchool(Long ecoleId);
	public void addStudentToClasse(Long studentId,Long classeId);
	public Double getMatiereAverage(Long matiereId,Long studentId);
	public List<Moyenne> getMoyennesList(Long studentId);
	
}
