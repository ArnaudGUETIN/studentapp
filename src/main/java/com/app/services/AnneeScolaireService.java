package com.app.services;

import com.app.entities.AnneeScolaire;

public interface AnneeScolaireService extends CommonService<AnneeScolaire> {
    public AnneeScolaire updateCurrentAnneeScolaire(Long id);
}
