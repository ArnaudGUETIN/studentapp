package com.app.services;

import java.util.List;

import com.app.entities.Cours;

public interface CoursService extends CommonService<Cours>{

	public List<Cours> getAllCoursOfClasse(Long classeId);
	public List<Cours> getAllCoursOfMatiere(Long matiereId);
	public void addCoursToClasse(Long coursId,Long classeId);
	public void addCoursToMatiere(Long coursId,Long matiereId);
}
