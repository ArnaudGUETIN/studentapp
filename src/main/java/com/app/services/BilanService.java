package com.app.services;

import com.app.entities.Matiere;

import java.math.BigDecimal;

public interface BilanService {

    BigDecimal getMoyenne();
    BigDecimal getMoyenne(Matiere matiere);

}
