package com.app.services;

import java.util.List;

import com.app.entities.Classe;

public interface ClasseService extends CommonService<Classe>{

	
	public List<Classe> getAllClassesOfSchool(Long ecoleId);
	public List<Classe> getAllClassesOfProfessor(Long professorId);
	public void addClasseToSchool(Long ecoleId,Long classeId);
	public void addClasseToProfessor(Long professorId,Long classeId);
	
}
