package com.app.services;

import java.util.List;


public interface CommonService<T> {

	public T getEntity(Long entityId);
	public T addEntity(T entity);
	public T updateEntity(T entity,Long entityId);
	public boolean removeEntity(Long entityId);
	public List<T> getAllEntities();

}
