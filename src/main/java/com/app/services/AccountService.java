package com.app.services;

import com.app.security.AppRole;
import com.app.security.AppUser;

public interface AccountService {

    public AppUser CreateUser(AppUser user);

    public AppRole CreateRole(AppRole role);

    public void addRoleToUser(String username, String roleName);

    public AppUser findUserByUsername(String username);

    public AppUser updateUser(String Username);
}
