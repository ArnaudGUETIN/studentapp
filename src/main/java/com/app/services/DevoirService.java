package com.app.services;

import java.util.List;

import com.app.entities.Devoir;

public interface DevoirService extends CommonService<Devoir>{

	public List<Devoir> getAllDevoirsOfMatiere(Long matiereId);
	public void addDevoirToMatiere(Long devoirId,Long matiereId,Long classeId);
}
