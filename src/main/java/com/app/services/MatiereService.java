package com.app.services;

import java.util.List;

import com.app.entities.Matiere;
import com.app.entities.Student;

public interface MatiereService extends CommonService<Matiere> {

	public List<Matiere> getAllMatieresOfClasse(Long classeId);
	public List<Matiere> getAllMatieresOfProfessor(Long professorId);
	public void addMatiereToProfessor(Long professorId,Long matiereId);
	public void addMatiereToClasse(Long classeId,Long matiereId);
	public List<Matiere> getMatieres(Student student);
	public List<Matiere> filterMatieresById(Student student,Long matiereId);
	
}
