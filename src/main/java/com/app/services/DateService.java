package com.app.services;

import java.time.LocalDateTime;
import java.util.Date;


public class DateService {

	public static Date convertToDateViaSqlTimestamp(LocalDateTime dateToConvert) {
	    return java.sql.Timestamp.valueOf(dateToConvert);
	}
}
