package com.app.services;

import com.app.entities.Matiere;
import com.app.entities.Student;

import java.math.BigDecimal;

public interface CalculMoyenneService {
    BigDecimal calculateStudentMoyenne(Long studentId);
    BigDecimal calculateStudentMoyenneForMatiere(Long studentId, Long matiereId);
}
